#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int main(){

    const int M = 5;//mida del vector constant.
    float v[M];//vector de floats amb mida M.

    printf("M:%d\n",M);
    //Càrrega de valors random
    srand(time(NULL));
    printf("Vector: [");
    for(int i=0; i<M; i++) {
        v[i] = (180.0 * rand()) / RAND_MAX;
        printf("%.2f", v[i]);
        if(i<M-1) {
            printf(", ");
        }
    }
    printf("]\n");

    //Recorrem el vector i fem el sin, cos i tan a cada posicio.
    for(int j=0; j<M; j++){
      
        printf("%.2f => cos: %.3f; sin: %.3f; tan: %.3f;\n",v[j],cos(v[j]),sin(v[j]),tan(v[j]));
    }
}